from django.views.generic import CreateView, UpdateView, ListView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import *



class AlumnoView(LoginRequiredMixin):
    model = Alumno

    def get_queryset(self):
        return Alumno.objects.filter()


class AlumnoList(AlumnoView, ListView):
    pass

class FamilyView(LoginRequiredMixin):
    model = Family

    def get_queryset(self):
        return Family.objects.filter()


class FamilyList(FamilyView, ListView):
    pass

