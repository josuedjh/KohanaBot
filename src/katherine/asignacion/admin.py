from functools import update_wrapper

from django.conf.urls import url
from django.contrib import admin
from django.db.models import Avg
from django.shortcuts import render, get_object_or_404

from .models import *
from .models import Family, Sala


@admin.register(Family)
class FamilyAdmin(admin.ModelAdmin):
    list_display = ['name', 'first_name', 'contact_email', 'contact_address']
    search_fields = ['name', 'first_name']


@admin.register(Sala)
class SalaAdmin(admin.ModelAdmin):
    list_display = ['name', 'notes']


@admin.register(Alumno)
class AlumnoAdmin(admin.ModelAdmin):
    list_display = ['name', 'family', 'sala']
    search_fields = ['contact_email']

    def get_urls(self):
        def wrap(view):
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            wrapper.model_admin = self
            return update_wrapper(wrapper, view)

        urlpatterns = super().get_urls()
        urlpatterns = [
                          url(r'^reporte/(.+)/$', wrap(self.alumno_evaluacion), name='alumno_evaluacion'),
                      ] + urlpatterns

        return urlpatterns

    def alumno_evaluacion(self, request, object_id):
        alumno = get_object_or_404(Alumno, id=object_id)

        indicadores = []
        for indicador in Indicador.objects.all():
            answer = indicador.answers.exclude(evaluacion__alumno=alumno).aggregate(value=Avg('value'))
            indicadores.append({'pregunta': indicador.question_text, 'value': answer['value']})

        context = {'alumno': alumno, 'indicadores': indicadores}

        return render(request, 'admin/alumno_evaluacion.html', context)
