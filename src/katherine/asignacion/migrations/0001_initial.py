# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-21 22:50
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alumno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(blank=True, upload_to='uploads', verbose_name='Fotografia')),
                ('name', models.CharField(max_length=150, verbose_name='Nombre')),
                ('second_last_name', models.CharField(help_text='introdusca los dos apellidos', max_length=140, verbose_name='Apellido materno y paterno')),
                ('contact_address', models.TextField(blank=True, verbose_name='Direccion de Contacto')),
                ('contact_email', models.EmailField(max_length=254, verbose_name='E-mail de contacto')),
            ],
        ),
        migrations.CreateModel(
            name='family',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', models.ImageField(blank=True, upload_to='uploads', verbose_name='Fotografia')),
                ('name', models.CharField(max_length=150, verbose_name='Nombre')),
                ('first_name', models.CharField(max_length=140, verbose_name='Apellido')),
                ('contact_address', models.TextField(blank=True, verbose_name='Direccion de Contacto')),
                ('contact_email', models.EmailField(max_length=254, verbose_name='E-mail de contacto')),
            ],
            options={
                'verbose_name': 'Familia',
                'verbose_name_plural': 'Familiares',
            },
        ),
        migrations.CreateModel(
            name='Sala',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='nombre de la sala')),
            ],
        ),
    ]
