# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-06-28 00:21
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('asignacion', '0018_auto_20170627_1936'),
    ]

    operations = [
        migrations.AlterField(
            model_name='alumno',
            name='family',
            field=models.ForeignKey(help_text='selecione los tutores', on_delete=django.db.models.deletion.CASCADE, to='asignacion.Family', verbose_name='Padres'),
        ),
    ]
