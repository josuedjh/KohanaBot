from django.db import models


class Family(models.Model):
    photo = models.ImageField(upload_to='uploads', blank=True, verbose_name='Fotografia')
    name = models.CharField(max_length=150, verbose_name='Nombre')
    first_name = models.CharField(max_length=140, verbose_name='Apellido')
    contact_address = models.TextField(blank=True, verbose_name='Direccion de Contacto')
    contact_email = models.EmailField(verbose_name='E-mail de contacto')

    class Meta:
        verbose_name = 'Familia'
        verbose_name_plural = 'Familiares'

    def __str__(self):
        return self.name

    def get_full_name(self):
        return f'{self.name}, {self.first_name}, {self.contact_email}'


class Sala(models.Model):
    name = models.CharField(max_length=150, verbose_name='nombre de la sala')
    notes = models.TextField(blank=True, verbose_name='Notas / Descripcion')

    def __str__(self):
        return self.name


class Alumno(models.Model):
    family = models.ForeignKey(Family, verbose_name='Padres', help_text='selecione los tutores')
    photo = models.ImageField(upload_to='uploads', blank=True, verbose_name='Fotografia')
    name = models.CharField(max_length=150, verbose_name='Nombre')
    sala = models.ForeignKey(Sala, verbose_name='Salas', help_text='selecione una sala..')

    def __str__(self):
        return self.name

    def get_full_name(self):
        return f'{self.name}, {self.name}'
