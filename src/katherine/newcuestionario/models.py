from django.db import models

from .builder import INPUT_CHOICES

# Create your models here.
class FormModel(models.Model):
    title = models.CharField(max_length=140, verbose_name='titulo', help_text='titulo del formulario..')
    slug = models.SlugField(max_length=100, blank=True, help_text='url')
    description = models.TextField(blank=True, help_text='una descripcion de la nueva evaluyacio...')

    def __str__(self):
        return self.title


class FormItem(models.Model):
    name = models.SlugField(blank=True, verbose_name='Nombre')
    form = models.ForeignKey(FormModel, related_name='items',verbose_name='Formulario', help_text='asigne un formulario..')
    field_type = models.CharField(choices=INPUT_CHOICES, max_length=20, verbose_name='tipo del form', help_text='selecciones el tipo del formulario...')
    order = models.PositiveIntegerField(db_index=True, blank=True, default=0, verbose_name='Orden', help_text='selecciones el orden del formulario.. ')

    class Meta:
        unique_together = (
            ('form', 'name'),
            ('form', 'order'))
        ordering = ('form', 'order',)

    def __str__(self):
        return f'{self.name}'

    def save(self, **kwargs):
        if not self.order or self.order == 0:
            self.order = self.form.items.all().count() + 1
        super().save(**kwargs)


class SurveyQuerySet(models.QuerySet):
    def published(self):
        from arrow import now
        return self.filter(
            status=self.model.PUBLISHED,
            starts_at__gte=now, ends_at__lte=now)


class SurveyManager(models.Manager.from_queryset(SurveyQuerySet)):
    pass


class Survey(models.Model):
    NEW = 'new'
    PUBLISHED = 'published'
    COMPLETED = 'completed'
    CLOSED = 'closed'
    STATUS_CHOICES = (
        (NEW, 'New'),
        (PUBLISHED, 'Published'),
        (COMPLETED, 'Completed'),
        (CLOSED, 'Closed')
    )

    name = models.CharField(max_length=140)
    status = models.CharField(choices=STATUS_CHOICES, max_length=12, default=NEW)
    form = models.ForeignKey(FormModel)

    starts_at = models.DateTimeField(blank=True, null=True)
    ends_at = models.DateTimeField(blank=True, null=True)

    objects = SurveyManager()

    def __str__(self):
        return self.name


class EntryQuerySet(models.QuerySet):
    def open(self):
        from django.utils.timezone import now
        today = now()
        return self.filter(
            status=self.model.IN_PROGRESS,
            survey__status=Survey.PUBLISHED,
            survey__starts_at__gte=today,
            survey__ends_at__lte=today)

    def published(self):
        return self.filter(
            status=self.model.NEW,
            survey__status=Survey.PUBLISHED)


class EntryManager(models.Manager.from_queryset(EntryQuerySet)):
    pass


class Entry(models.Model):
    NEW = 'new'
    IN_PROGRESS = 'in-progress'
    EXPIRED = 'expired'
    COMPLETED = 'completed'

    STATUS_CHOICES = (
        (NEW, 'New'),
        (IN_PROGRESS, 'In progress'),
        (EXPIRED, 'Expired'),
        (COMPLETED, 'Completed'),
    )

    survey = models.ForeignKey(Survey, null=True, blank=True)
    status = models.CharField(choices=STATUS_CHOICES, default=NEW, verbose_name='Estado', max_length=12)
    reference = models.CharField(max_length=255, blank=True, verbose_name='Referencia',)
    full_name = models.CharField(max_length=140, blank=True, verbose_name='Nombre completo')

    objects = EntryManager()

    class Meta:
        verbose_name_plural = 'entries'

    def __str__(self):
        return f'{self.survey} {self.reference}'
