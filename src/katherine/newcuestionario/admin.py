from django.contrib import admin

# Register your models here.

from .models import FormModel, FormItem, Survey, Entry


class FormItemInline(admin.StackedInline):
    model = FormItem
    extra = 0


@admin.register(FormModel)
class FormModelAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    inlines = [FormItemInline]


@admin.register(FormItem)
class FormItemAdmin(admin.ModelAdmin):
    list_display = ('name', 'form', 'field_type')
    list_filter = ('form',)
    search_fields = ('name',)
    raw_id_fields = ('form',)


@admin.register(Survey)
class SurveyAdmin(admin.ModelAdmin):
    list_display = ('name', 'status', 'starts_at', 'ends_at')
    list_filter = ('status', 'starts_at', 'ends_at')
    search_fields = ('name',)


@admin.register(Entry)
class EntryAdmin(admin.ModelAdmin):
    list_display = ('reference', 'survey', 'status')
    raw_id_fields = ('survey',)
    search_fields = ('reference', 'full_name')
