from django import forms

INPUT_TYPES = {
    'BooleanField': forms.BooleanField,
    'AcceptField': forms.BooleanField,
    'YesNoField': forms.BooleanField,
    'CharField': forms.CharField,
    'MatrixField': forms.IntegerField,  # Satisfaction matrix
    'PhoneField': forms.CharField,
    'TextField': forms.CharField,
    'ChoiceField': forms.ChoiceField,
    'DateField': forms.DateField,
    'DateTimeField': forms.DateTimeField,
    'DurationField': forms.DurationField,
    'EmailField': forms.EmailField,
    'FileField': forms.FileField,
    'PriceField': forms.FloatField,
    'FloatField': forms.FloatField,
    'ImageField': forms.ImageField,
    'IntegerField': forms.IntegerField,
    'MultipleChoiceField': forms.MultipleChoiceField,
    'TimeField': forms.TimeField,
    'URLField': forms.URLField,

    'PageBreak': forms.CharField,
    'Text': forms.CharField,
    'Heading': forms.CharField,
    'Process': forms.CharField,
}

INPUT_CHOICES = list((value, value) for value in INPUT_TYPES.keys())