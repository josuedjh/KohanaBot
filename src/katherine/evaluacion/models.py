from django.contrib.auth.models import User
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.db import models

from katherine.asignacion.models import Alumno
from katherine.core.models import Chat


class Indicador(models.Model):
    name = models.CharField(max_length=100)  # Usaremos esto como categoria
    question_text = models.CharField(
        max_length=255,
        verbose_name='Pregunta a realizar para evaluar este indicador.')

    def __str__(self):
        return f'{self.name}: {self.question_text}'


class Evaluacion(models.Model):
    user = models.ForeignKey(User, verbose_name='Tutor')
    alumno = models.ForeignKey(Alumno)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    notified = models.DateTimeField(null=True, blank=True)
    completed = models.DateTimeField(null=True, blank=True)

    class Meta:
        verbose_name = 'Evaluacion'
        verbose_name_plural = 'Evaluaciones'

    def __str__(self):
        return f'{self.user} evalua a {self.alumno}'

    def clean(self):
        super().clean()
        try:
            user_chat = self.user.chat
        except ObjectDoesNotExist:
            raise ValidationError('Solo tutores con chat activo pueden realizar evaluaciones')


class Answer(models.Model):
    ANSWER_VALUES = (
        (1, 'Bajo'),
        (2, 'Regular'),
        (3, 'Bueno')
    )
    evaluacion = models.ForeignKey(Evaluacion, related_name='respuestas')
    indicator = models.ForeignKey(Indicador)
    value = models.IntegerField(choices=ANSWER_VALUES)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = (('evaluacion', 'indicator'),)
