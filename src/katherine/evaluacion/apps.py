from django.apps import AppConfig


class EvaluacionConfig(AppConfig):
    name = 'katherine.evaluacion'