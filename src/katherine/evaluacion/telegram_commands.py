from django.utils.timezone import now
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup

from katherine.asignacion.models import Alumno
from katherine.core.models import Chat
from katherine.evaluacion.models import Evaluacion, Indicador, Answer


def start_command(update: Update):
    chat = update.effective_chat

    chat_room, created = Chat.objects.get_or_create(
        chatid=chat.id, defaults={'json_data': update.to_json()})

    if created:
        chat_room.send_message('Hola')
        chat_room.send_message(f"""
         Por favor, avisa al administrador que asocie esta cuenta de chat a tu cuenta en el Kinder.

         Tu codigo es `{chat_room.id}`
         """)
    else:
        if chat_room.user_id:
            chat_room.send_message(f'Hola, esta chat esta asociado a *{chat_room.user}*')
        else:
            chat_room.send_message(f"""
             Hola, esta chat todavia no esta asociado, avisa al administrador.

             Tu codigo es `{chat_room.id}`
             """)


def list_alumnos_command(update: Update):
    chat = update.effective_chat

    chat_room, created = Chat.objects.get_or_create(
        chatid=chat.id, defaults={'json_data': update.to_json()})

    if chat_room.user_id:
        alumnos = Alumno.objects.all()
        groups = []

        for alumno in alumnos:
            groups.append([
                InlineKeyboardButton(text=alumno.get_full_name(), callback_data=f'E:{alumno.id}'),
            ])

        inline = InlineKeyboardMarkup(groups)
        chat_room.send_message("Esta es la lista de los estudiantes", reply_markup=inline)


def start_evaluation(update: Update, alumno: Alumno):
    chat = update.effective_chat

    chat_room, created = Chat.objects.get_or_create(
        chatid=chat.id, defaults={'json_data': update.to_json()})

    evaluacion, created = Evaluacion.objects.get_or_create(user=chat_room.user, alumno=alumno)

    chat_room.send_message(f'Se inicia la evaluacion para el alumno, {alumno.get_full_name()}')

    siguiente_pregunta(chat_room, evaluacion)


def answer_evaluacion(update, evaluacion, indicador, valor):
    chat = update.effective_chat

    chat_room, created = Chat.objects.get_or_create(
        chatid=chat.id, defaults={'json_data': update.to_json()})

    answer, created = Answer.objects.get_or_create(
        evaluacion=evaluacion,
        indicator=indicador,
        defaults={'value': valor})

    pregunta = siguiente_pregunta(chat_room=chat_room, evaluacion=evaluacion)

    if not pregunta:
        evaluacion.completed = now()
        evaluacion.save(update_fields=['completed'])

        chat_room.send_message('Se termino la evaluacion, elija otro estudiante.')


def siguiente_pregunta(chat_room, evaluacion):
    indicador_sin_respuesta = Indicador.objects.exclude(answer__evaluacion=evaluacion).first()
    evaluacion, created = Evaluacion.objects.get_or_create(user=chat_room.user, alumno=evaluacion.alumno)


    if not indicador_sin_respuesta:
        return False

    reply_buttons = InlineKeyboardMarkup([[
        InlineKeyboardButton(text='Bajo',
                             callback_data=f'R:{evaluacion.id}:{indicador_sin_respuesta.id}:1'),
        InlineKeyboardButton(text='Regular',
                             callback_data=f'R:{evaluacion.id}:{indicador_sin_respuesta.id}:2'),
        InlineKeyboardButton(text='Bueno',
                             callback_data=f'R:{evaluacion.id}:{indicador_sin_respuesta.id}:3'),
    ]])

    chat_room.send_message(indicador_sin_respuesta.question_text, reply_markup=reply_buttons)
    return True
