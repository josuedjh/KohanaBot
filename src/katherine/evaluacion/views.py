import json
import re

from django.http import HttpRequest, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from telegram import Bot, Update

from katherine.asignacion.models import Alumno
from katherine.evaluacion.models import Evaluacion, Indicador
from katherine.evaluacion.telegram_commands import start_command, list_alumnos_command, start_evaluation, \
    answer_evaluacion

bot = Bot('326357975:AAGU3FpmHt5Of3Owgwof4uO0tsX-gnOCWUk')

evaluacion_action = re.compile(r'^E\:(?P<alumno_id>\d+)$')
answer_action = re.compile(r'^R\:(?P<evaluacion_id>\d+):(?P<indicador_id>\d+):(?P<valor>\d)$')


@csrf_exempt
def webhook_view(request: HttpRequest):
    if request.method == 'POST':
        data = json.loads(request.body)
        update = Update.de_json(data, bot)
        message = update.effective_message

        if message.text == '/start':
            start_command(update)
        elif message.text == '/alumnos':
            list_alumnos_command(update)

        if update.callback_query:
            eval_match = evaluacion_action.match(update.callback_query.data)
            answer_match = answer_action.match(update.callback_query.data)

            if eval_match:
                alumno_id = eval_match.groupdict().get('alumno_id')
                alumno = Alumno.objects.get(id=alumno_id)
                start_evaluation(update, alumno)
            elif answer_match:
                evaluacion_id = answer_match.groupdict().get('evaluacion_id')
                indicador_id = answer_match.groupdict().get('indicador_id')
                valor = answer_match.groupdict().get('valor')

                evaluacion = Evaluacion.objects.get(id=evaluacion_id)
                indicador = Indicador.objects.get(id=indicador_id)

                answer_evaluacion(update, evaluacion, indicador, valor)

        return HttpResponse('ok', status=200)

    return HttpResponse('error', status=400)
