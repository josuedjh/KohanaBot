from django.contrib import admin

from .models import Evaluacion, Indicador, Answer


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 0
    radio_fields = {
        'value': admin.VERTICAL
    }


@admin.register(Evaluacion)
class EvaluacionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'created')
    raw_id_fields = ['alumno', 'user']
    inlines = [AnswerInline,]


@admin.register(Indicador)
class IndicadorAdmin(admin.ModelAdmin):
    list_display = ('name', 'question_text')
    list_filter = ('name',)
    search_fields = ['name', 'question_text']


@admin.register(Answer)
class AnswerAdmin(admin.ModelAdmin):
    list_display = ('evaluacion', 'created')
    list_filter = ('created',)

