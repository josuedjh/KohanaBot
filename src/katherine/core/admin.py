from django.contrib import admin

from katherine.core.models import Chat


@admin.register(Chat)
class ChatAdmin(admin.ModelAdmin):
    list_display = ('id', 'chatid', 'user', 'created')

