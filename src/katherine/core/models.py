from django.contrib.auth.models import User
from django.db import models
from telegram import Bot

bot = Bot('456220994:AAFhQ05BM5ldRN1Vl3OvXh2uF4WpG6lySAw')


class Chat(models.Model):
    chatid = models.IntegerField()
    user = models.OneToOneField(User, null=True, blank=True, related_name='chat')
    json_data = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.chatid} {self.user}'


    def send_message(self, text, **kwargs):
        kwargs.setdefault('parse_mode', 'markdown')
        return bot.send_message(self.chatid, text, **kwargs)
