from django.db import models

# Create your models here.
from katherine.asignacion.models import Sala


class Tutora(models.Model):
    name = models.CharField(max_length=150, verbose_name='Maestra', help_text='nombre de la perosona')
    second_last_name = models.CharField(max_length=140, verbose_name='Apellido materno y paterno', help_text='introdusca los dos apellidos')
    contact_address = models.TextField(blank=True, verbose_name='Direccion de Contacto')
    sala = models.ForeignKey(Sala, verbose_name='Salas', help_text='selecione una sala..')


    def __str__(self):
        return self.name


    def get_full_name(self):
        return f'{self.name}, {self.name}, {self.second_last_name}'


class Media(models.Model):
    type = models.CharField(max_length=150, verbose_name='typo de archivo', help_text='seleciones el formato del archivo')
    document = models.FileField(upload_to='documents/')
    note = models.CharField(max_length=150, verbose_name='nota')
    tutora = models.ForeignKey(Tutora, verbose_name='Tutora')

    def __str__(self):
        return self.type
