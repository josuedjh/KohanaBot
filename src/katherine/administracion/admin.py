from django.contrib import admin

# Register your models here.
from .models import  Tutora, Media


@admin.register(Tutora)
class TutoraAdmin(admin.ModelAdmin):
    list_display = ['name', 'second_last_name', 'contact_address', 'sala']
    list_display_links = None
    list_filter = ('name', 'contact_address')
    search_fields = ['name', 'first_name']
    raw_id_fields = ['sala',]


@admin.register(Media)
class MediayAdmin(admin.ModelAdmin):
    list_display = ['type', 'note']
    search_fields = ['type', 'note']
