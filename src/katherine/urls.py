from django.conf.urls import url, include
from django.conf import settings
from django.contrib import admin
from django.views.generic.base import TemplateView

from katherine.evaluacion.views import webhook_view
from .core.views import UserSettings, RegistrationView
from .asignacion.views import *

urlpatterns = [
    url(r'^jet/', include('jet.urls', 'jet')),
    url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),
    url(r'^admin/', admin.site.urls),
    url(r'^admin/docs/', include('django.contrib.admindocs.urls')),
    url(r'^accounts/$', UserSettings.as_view(), name='user_settings'),
    url(r'^accounts/register/$', RegistrationView.as_view(), name='user_registration'),
    url(r'^accounts/', include('django.contrib.auth.urls')),

    url(r'^webhook/$', webhook_view, name='webhook_view'),
    url(r'^alumno/$', AlumnoList.as_view(), name='alumno_landing'),
    url(r'^family/$', FamilyList.as_view(), name='family_landing'),
    url(r'^evaluacion/$', FamilyList.as_view(), name='evaluacion_landing'),

    url(r'^$', TemplateView.as_view(template_name='landing.html'), name='landing'),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    from django.contrib.staticfiles.views import serve as static_serve

    staticpatterns = static(settings.STATIC_URL, view=static_serve)
    mediapatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
    urlpatterns = staticpatterns + mediapatterns + urlpatterns
