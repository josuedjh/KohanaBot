import {Component} from "react";


class Input extends Component {
    constructor() {
        super()
        this.state = {edit: false}
    }

    handleDblClick(event) {
        this.setState({'edit': true})
    }

    handleOnBlur(event) {
        this.setState({'edit': false})
        this.props.handleSave(this.props.name, this.refs.input.value)
    }

    render() {
        let content;
        if (this.state.edit) {
            content = <input
                ref="input"
                name={this.props.name}
                type="text"
                defaultValue={this.props.defaultValue}
                className="form-control"
                onBlur={this.handleOnBlur.bind(this)} />
        } else {
            const editMsg = <span>
                <span className="fa fa-pencil" /> 
                Editar
            </span>;

            content = <span onDoubleClick={this.handleDblClick.bind(this)} className="editable">
                {this.props.defaultValue !== '' ? this.props.defaultValue : editMsg}
            </span>
        }

        return <div className="form-group">
            {content}
        </div>

    }
}


class Invoice extends Component {
    saveInvoice(event) {

    }

    onSaveField(fieldName, fieldValue) {
        console.log('as')
    }

    render() {

        return <div className="paper">
            <h2>Invoice {this.props.invoice.ref}</h2>

            <div className="row">
                <div className="col-sm">
                    <p>
                    <strong>
                    </strong>
                    </p>
                    <p>
                    </p>
                </div>
                <div className="col-sm">
                    <label htmlFor="customer_name">Nombre del Cliente</label>
                    <Input 
                        defaultValue={this.props.invoice.customer_name}
                        handleSave={this.onSaveField.bind(this)}
                        name="customer_name" />
                </div>
            </div>
            <hr />
            <table className="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Price</th>
                        <th>Total</th>
                        <th />
                    </tr>
                </thead>
                <tbody>
                    {this.props.invoice.entries.map(function(entry, key) {
                        return <tr key={key}>
                            <td>{entry.description}</td>
                            <td>{entry.quantity}</td>
                            <td>{entry.price}</td>
                            <td>{entry.quantity * entry.price}</td>
                            <td>
                               <span className="fa fa-trash-o" />
                            </td>
                        </tr>
                    })}
                   </tbody>
            </table>
            <button onClick={this.saveInvoice.bind(this)} className="btn btn-primary">
                Guardar
            </button>
         </div>
    }
}

export default Invoice;
